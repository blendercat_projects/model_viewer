extends CanvasLayer

const DEFAULT_MODEL_PATH = "res://default_models/cube.escn"

onready var fbrowser : PopupDialog
onready var ui_viewport_container : ViewportContainer
onready var ui_viewport : Viewport
onready var model_viewport_container : ViewportContainer
onready var model_viewport : Viewport
onready var browser_button : Button
onready var default_object_position : Position3D
onready var current_camera : Camera

var browser_visible : bool = false
var file_selected_path_parts
var current_model_object = null
var current_screen_size : Vector2 = Vector2.ZERO


func _ready():
	fbrowser = get_node("VC_UI/V_UI/Panel/FBrowser")
	ui_viewport_container = get_node("VC_UI")
	ui_viewport = get_node("VC_UI/V_UI")
	model_viewport_container = get_node("VC_Model")
	model_viewport = get_node("VC_Model/V_M")
	browser_button = get_node("VC_UI/V_UI/Panel/BrowseButton")
	fbrowser.get_cancel().connect("pressed", self, "_on_cancel")
	default_object_position = get_node("VC_Model/V_M/Default_Object_Position")
	#set_viewports_sizes()
	#current_camera = get_node("VC_Model/V_M/Camera")
	load_default_model()
	enable_browser()


func delete_last_object():
	get_default_object()
	current_model_object.queue_free()


func disable_browse_button():
	browser_button.disabled = true


func enable_browse_button():
	browser_button.disabled = false


func enable_browser():
	show_browser()
	hide_viewport()
	disable_browse_button()


func enable_viewport():
	show_viewport()
	hide_browser()
	enable_browse_button()


func get_default_object():
	current_model_object = default_object_position.get_child(0)


func get_screen_size():
	return OS.get_screen_size()


func hide_browser():
	browser_visible = false
	fbrowser.hide()


func hide_viewport():
	model_viewport_container.visible = false


func import_model():
	var res_path : String
	var dir = Directory.new()
	dir.copy(file_selected_path_parts[0] + "/" + file_selected_path_parts[1], "res://imported_models/" + file_selected_path_parts[1])
	res_path = "res://imported_models/" + file_selected_path_parts[1]
	
	return res_path


func load_default_model():
	var inventory_scene = load(DEFAULT_MODEL_PATH)
	current_model_object = inventory_scene.instance()
	current_model_object.name = "current_model_object"
	default_object_position.add_child(current_model_object)
	print(current_model_object)


func load_selected_model():
	var fileselected_respath : String = import_model()
	var mesh = load(fileselected_respath)
	if mesh == null:
		load_default_model()
	else:
		current_model_object = mesh.instance()
		current_model_object.name = "current_model_object"
		default_object_position.add_child(current_model_object)
		print(current_model_object)


func set_viewports_sizes():
	set_uiviewport_size()
	set_modelviewport_size()


func set_modelviewport_size():
	var w_size : Vector2 = OS.get_window_size()
	var initial_size : Vector2 = model_viewport.size


func set_uiviewport_size():
	var w_size : Vector2 = OS.get_window_size()
	ui_viewport_container.rect_size = w_size
	ui_viewport.size = w_size
	#print(w_size)


func show_browser():
	browser_visible = true
	fbrowser.popup()


func show_viewport():
	model_viewport_container.visible = true


func _on_BrowseButton_pressed():
	enable_browser()


func _on_cancel():
	enable_viewport()


func _on_FBrowser_file_selected(path):
	delete_last_object()
	file_selected_path_parts = path.rsplit("/", false, 1)
	load_selected_model()
	enable_viewport()
