extends Viewport


export var turning_speed = 0.01
export var zooming_speed = 0.01
export var zooming_speed_mouse = 2.0


onready var gamemanager
onready var viewport_camera : Camera

var touch0 : bool
var touch1 : bool
var control_pressed : bool
var drag0 : Vector2
var drag1 : Vector2
var last_drag_realitve : float

var multitouch : bool

func _ready():
	gamemanager = get_node("../..")
	viewport_camera = get_node("Camera")

func auto_set_multitouch():
	multitouch = (touch0 && touch1)


func get_touches(ev):
	match ev.get_index():
		0:
			if (ev is InputEventScreenTouch):
				touch0 = ev.pressed
			elif ev is InputEventScreenDrag:
				drag0 = ev.get_relative()
		1:
			if (ev is InputEventScreenTouch):
				touch1 = ev.pressed
			elif ev is InputEventScreenDrag:
				drag1 = ev.get_relative()


func rotating(ev):
	if ev is InputEventScreenDrag:
		var horiz = ev.get_relative().x
		var vert = ev.get_relative().y
		# ROTATE
		gamemanager.default_object_position.rotate(Vector3.UP, horiz * turning_speed)
		gamemanager.default_object_position.rotate(Vector3.RIGHT, vert * turning_speed)


func zooming(ev):
	if ev is InputEventScreenDrag:
		var drag_sum = drag0.distance_to(drag1)
		if drag_sum < last_drag_realitve:
			viewport_camera.fov += (drag_sum * zooming_speed)
		else:
			viewport_camera.fov -= (drag_sum * zooming_speed)
		last_drag_realitve = drag_sum


func zooming_with_mouse(up : bool):
	if up:
		viewport_camera.fov -= zooming_speed_mouse
	else:
		viewport_camera.fov += zooming_speed_mouse


func _input(event):
	if gamemanager.browser_visible:
		return
	
	# Managing input
	## First check ctrl key (for computer execution)
	if (event.is_action_pressed("ui_control")):
		control_pressed = true
	elif (event.is_action_released("ui_control")):
		control_pressed = false
	elif (event.is_action_released("ui_middle_mouse_down")):
		zooming_with_mouse(false)
	elif (event.is_action_released("ui_middle_mouse_up")):
		zooming_with_mouse(true)
	elif (event is InputEventScreenTouch) or (event is InputEventScreenDrag):
		## Finally, check inputs
		get_touches(event)
		auto_set_multitouch()
		if multitouch:
			zooming(event)
		else:
			rotating(event)
